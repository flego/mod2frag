# Model to shader file conversion and visualization tool (WIP) 

## Pre-built files

see [releases](../../releases)

## Build instructions 

### MacOS

1. Install [SDL2 framework](https://www.libsdl.org/) to /Library/Frameworks/
2. Download source code
3. Temporary workwaround: Adjust ``$INCLUDE`` in the Makefile to point at the correct locations
4. ``cd /path/to/source/ && make``

### Linux

1. Install [SDL2 framework](https://www.libsdl.org/) and [GTK-3](https://www.gtk.org/) with your package manager
2. Download source code
3. Temporary workwaround: Adjust ``$INCLUDEADDLINUX`` in the Makefile to point at the correct locations
4. ``cd /path/to/source/ && make run_linux_x64``

### Windows (cross compile using mingw)

1. Install mingw on your host
2. Install [SDL2 framework for mingw](https://www.libsdl.org/)
3. Adjust ``$WINCC``, ``$MINGW_PATH``, ``$MINGW_INCLUDE``, ``$MINGW_LIB`` in the Makefile appropriately
4. ``cd /path/to/source/ && make bundle_win_x64``

## Roadmap

- [x] render fragment shader files
- [ ] models: cgns import
- [ ] upload render to shadertoy functionality
