#include <iostream>
#ifdef __APPLE__
    #include <SDL.h> // For macOS
    #include <CoreFoundation/CoreFoundation.h>
#else
    #include <SDL2/SDL.h> // For other platforms
#endif

// Include GLAD
#include <glad/glad.h>
#include <fstream>
#include <sstream>
#include <string>
#include <filesystem>
// #include <CoreFoundation/CoreFoundation.h>  not mingw
#ifdef _WIN32
#include <windows.h>
#endif
#include "unistd.h"
#include "imgui.h"

#include "menuWindow.h"
#include "glslWindow.h"


// Config parameters
int windowInitialX = 640;
int windowInitialY = 480;

std::filesystem::path currentPath = std::filesystem::current_path();
std::filesystem::path shaderfilesDirName("shaderfiles");
std::filesystem::path shaderfilesDirPath = currentPath / shaderfilesDirName;
std::filesystem::path defaultShaderName("spiral.frag");
std::filesystem::path shaderPath = currentPath / shaderfilesDirName / defaultShaderName;
std::filesystem::path vertexShaderName("vertex_shader.glsl");
std::filesystem::path vertexShaderPath = currentPath / vertexShaderName;

// Convert filesystem path to string in a way that is safe for both Windows and Unix-like systems.
std::string path_to_string(const std::filesystem::path& path) {
    #ifdef _WIN32
    // On Windows, convert from wchar_t to UTF-8
    std::wstring wstr = path.wstring();
    int size_needed = WideCharToMultiByte(CP_UTF8, 0, &wstr[0], (int)wstr.size(), NULL, 0, NULL, NULL);
    std::string strTo(size_needed, 0);
    WideCharToMultiByte(CP_UTF8, 0, &wstr[0], (int)wstr.size(), &strTo[0], size_needed, NULL, NULL);
    return strTo;
    #else
    // On Unix-like systems, return the string representation directly
    return path.string();
    #endif
}

void InitPaths() {
    currentPath = std::filesystem::current_path();
    shaderfilesDirPath = currentPath / shaderfilesDirName;
    shaderPath = shaderfilesDirPath / defaultShaderName;
    vertexShaderPath = currentPath / vertexShaderName;
}

std::string readShaderFromFile(const std::string& filename) {
    std::ifstream file(filename);
    if (!file.is_open()) {
        // Handle error: file not found or other issues
        std::cout << "Error: fragment shader file could not be read\n";
        return "";
    }
    
    std::stringstream buffer;
    buffer << file.rdbuf();
    return buffer.str();
}

#ifdef _WIN32
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PSTR lpCmdLine, INT nCmdShow) {
    main(0, NULL);
}
#endif

int main(int argc, char* argv[]){ 

    #ifdef __APPLE__
    CFBundleRef mainBundle = CFBundleGetMainBundle();
    CFURLRef resourcesURL = CFBundleCopyResourcesDirectoryURL(mainBundle);
    char path[PATH_MAX];
    if (!CFURLGetFileSystemRepresentation(resourcesURL, TRUE, (UInt8 *)path, PATH_MAX)) {
        // Error: failed to get file system representation
    }
    CFRelease(resourcesURL);

    // Change the working directory to the .app's resources path
    chdir(path);
    #endif
    
    // Now you can use relative paths to access resources in the .app bundle.
    // currentPath = path;

    InitPaths();
    std::cout << "Current Path: " << currentPath << std::endl;
    



    do {

        // OpenGL setup the graphics context
        SDL_Window* glslWindow = nullptr;
        SDL_GLContext glslContext;
        SDL_GL_MakeCurrent(glslWindow, glslContext);
        InitGLSLContext(glslWindow, glslContext, shaderPath, windowInitialX, windowInitialY);
        // SDL_GL_SwapWindow(glslWindow);

        // Initialize ImGUI
        SDL_Window * menuWindow = nullptr;
        SDL_GLContext menuContext;
        ImGuiContext* imGUIContext;
        SDL_GL_MakeCurrent(menuWindow, menuContext);
        InitializeImGui(menuWindow, menuContext, imGUIContext);
        // SDL_GL_SwapWindow(menuWindow);
        
        // SHADER CODE
        // After SDL and OpenGL initialization

        std::string vertexShaderCode = readShaderFromFile(path_to_string(vertexShaderPath));
        std::string fragmentShaderCode = "";

        if (strlen(path_to_string(shaderPath).c_str()) > 1) {
            std::cout << "strlen(shaderPath): " << strlen(path_to_string(shaderPath).c_str()) << "\n";
            std::cout << "Opening file: " << shaderPath << "\n";
            fragmentShaderCode = readShaderFromFile(path_to_string(shaderPath));
        } 
        else {
            std::cout << "No shader path specified. Quitting.\n";
            return 0;  // TODO clean up
        }
        shaderPath = "";
        
        if (vertexShaderCode.empty() || fragmentShaderCode.empty()) {
            // Handle error: couldn't read shader files
            // print error message
            std::cout << "Error: couldn't read vertex or fragment shader file(s). Are the paths correct?\n";
            std::cout << "vertexShaderCode: " << vertexShaderCode << std::endl;
            std::cout << "fragmentShaderCode: " << fragmentShaderCode << std::endl;
            return -1;
        }

        SDL_GL_MakeCurrent(glslWindow, glslContext);

        GLuint shaderProgram = CreateShaderProgram(vertexShaderCode.c_str(), fragmentShaderCode.c_str());
        if (shaderProgram < 0) {
            // Handle error: shader program couldn't be created
            // print error message
            std::cout << "Error: shader program couldn't be created\n";
            return -1;
        }

        // call StartGLSLWindow with openFilename for dialog to show shaderfilesDirPath
        std::string openFilename = path_to_string(shaderfilesDirPath);
        StartGLSLWindow(glslWindow, glslContext, windowInitialX, windowInitialY, 
                    shaderProgram, menuWindow, menuContext, imGUIContext, openFilename);
        shaderPath = path_to_string(openFilename.c_str());
        std::cout << "shaderPath: " << shaderPath << "\n";
        if (strlen(path_to_string(shaderPath).c_str()) == 0) {
            std::cout << "No shader path specified. Quitting.\n";
            return 0;
        }


        // CLEANUP
        // CleanupGLSL();  // called in glslWindow.cpp
        DeleteImGui();
        SDL_GL_DeleteContext(menuContext);
        SDL_DestroyWindow(menuWindow);
        
        SDL_Quit();

    } while (strlen(path_to_string(shaderPath).c_str()) > 0);

    return 0;
}
