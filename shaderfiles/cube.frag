#version 150

out vec4 fragColor;

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

uniform vec2 iResolution;
uniform float iTime;
uniform float iTimeDelta;
uniform int iFrame;
uniform vec4 iMouse;
uniform sampler2D iChannel0;
uniform sampler2D iChannel1;
uniform sampler2D iChannel2;
uniform sampler2D iChannel3;
uniform vec4 iDate;
uniform float iSampleRate;

void mainImage(out vec4, in vec2);
void main(void) { mainImage(fragColor,gl_FragCoord.xy); }

// TODO move lines above outside this file, include?

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// distance from ray to point
float DistLine(vec3 ro, vec3 rd, vec3 p) {
    return length(cross(p - ro, rd)) / length(rd);
}

float DrawPoint(vec3 ro, vec3 rd, vec3 p) {
    float d = DistLine(ro, rd, p);
    d = smoothstep(.06,.05,d);
    return d;
    
}


void mainImage(out vec4 fragColor, in vec2 fragCoord)
{
    vec2 uv = fragCoord.xy / iResolution.xy;
    uv -= .5;
    uv.x *= iResolution.x / iResolution.y;

    vec2 mouse = iMouse.xy / iResolution.xy;
    
    //vec3 camPos = vec3(sin(iTime), .5, cos(iTime));
    vec3 camPos = vec3( 2.5 * sin(10. * mouse.x) + .5, 
                        5.  * -sin(1. * mouse.y - 3.14/8) + 1.25, 
                        2.5 * cos(10. * mouse.x) + .5);
    vec3 lookAt = vec3(.5, .5, .5);
    float zoom = 1.;
    
    
    // r and u of pixel on display plane
    vec3 f = normalize(lookAt - camPos);
    vec3 r = normalize(cross(vec3(0., 1., 0.), f));
    vec3 u = cross(f, r);
    
    // ray eye -> pixel on plane
    vec3 ro = camPos;
    vec3 c = ro + zoom*f;
    vec3 i = c + uv.x*r + uv.y*u;
    vec3 rd = i - ro;
    
    
    // draw things
    float d = .0;
    d += DrawPoint(ro, rd, vec3(0., 0., 0.));
    d += DrawPoint(ro, rd, vec3(0., 0., 1.));
    d += DrawPoint(ro, rd, vec3(0., 1., 0.));
    d += DrawPoint(ro, rd, vec3(0., 1., 1.));
    d += DrawPoint(ro, rd, vec3(1., 0., 0.));
    d += DrawPoint(ro, rd, vec3(1., 0., 1.));
    d += DrawPoint(ro, rd, vec3(1., 1., 0.));
    d += DrawPoint(ro, rd, vec3(1., 1., 1.));
    
    fragColor = vec4(d);
}