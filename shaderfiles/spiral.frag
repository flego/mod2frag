#version 150

out vec4 fragColor;

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

uniform vec2 iResolution;
uniform float iTime;
uniform float iTimeDelta;
uniform int iFrame;
uniform vec4 iMouse;
uniform sampler2D iChannel0;
uniform sampler2D iChannel1;
uniform sampler2D iChannel2;
uniform sampler2D iChannel3;
uniform vec4 iDate;
uniform float iSampleRate;

void mainImage(out vec4, in vec2);
void main(void) { mainImage(fragColor,gl_FragCoord.xy); }

// TODO move lines above outside this file, include?

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#define S(a, b, t) smoothstep(a,b,t)

float rand(float co1, float co2){
    return fract(sin(dot(vec2(co1,co2), vec2(12.9898, 78.233))) * 43758.5453 * 
        (iMouse.x * iMouse.y) / (iResolution.x * iResolution.y));
}

//float rr(float r){
//    return rand(0., r);
//}

struct ray {
    vec3 o, d;
};

ray GetRay(vec2 uv, vec3 camPos, vec3 lookAt, float zoom) {
    ray a;
    a.o = camPos;

    // r and u of pixel on display plane
    vec3 f = normalize(lookAt - camPos);
    vec3 r = normalize(cross(vec3(0., 1., 0.), f));
    vec3 u = cross(f, r);
    
    // ray eye -> pixel on plane
    vec3 c = a.o + zoom*f;
    vec3 i = c + uv.x*r + uv.y*u;
    
    a.d = normalize(i - a.o);
    
    return a;
}

// find closest point on ray to another point outside ray
vec3 ClosestPoint(ray r, vec3 p) {
    return r.o + max(0., dot(p - r.o, r.d)) * r.d;
}

// distance from ray to point
float DistRay(ray r, vec3 p) {
    return length(p - ClosestPoint(r, p));
}

vec3 Bokeh(ray r, vec3 p, float size, float blur, vec3 col) {
    float d = DistRay(r, p);
    
    float c = S(size, size*(1.-blur), d);
    c *= mix(.5 + (iMouse.x * iMouse.y) / (iResolution.x * iResolution.y), 
            1., S(size*.8, size, d)); // mix applies operation mainly on size>.7
    
    col *= c;

    return col;
}


void mainImage(out vec4 fragColor, in vec2 fragCoord)
{
    vec2 uv = fragCoord.xy / iResolution.xy;
    uv -= .5;
    uv.x *= iResolution.x / iResolution.y;
    
    vec3 camPos = vec3(0., .2, 0.);
    //vec3 camPos = vec3(.5 + 2*sin(iTime) ,1.5, .5 + 2*cos(iTime));
    vec3 lookAt = vec3(.0, .2, 1.);
    float zoom = 1.;
    
    ray r = GetRay(uv, camPos, lookAt, zoom);
    
    // draw things
    float t = iTime * 60. / iResolution.y;
    float s = 1./10.;
    vec3 m = vec3(.0);   // sum up to frame
    
    for(float i = .1; i<1.1; i+=s) {
        float ti = fract(t+i);
        //float ti = t+i;
        
        // loading circle:
        //vec3 p = vec3(2.*sin(6.3*i), 2.*cos(6.3*i), 20.-100.*ti*exp(-ti*6.));
        
        // accelerating
        vec3 p = vec3(  sin(6.3*i)*.1*exp(3.*ti), 
                        cos(6.3*i)*.1*exp(3.*ti), 
                        (60.)/exp(ti*5.));
        
        //orange col: vec3(1.0, .7, .25))
        //m += Bokeh(r, p, .6, .1, vec3(1.0, .7, .25));
        
        m += Bokeh(r, p, .6, .1, vec3(rand(2.,8.*i), rand(i,2.*i), rand(2.*i,1.*i))); 
        m *= 1.02;
    }
    
    
    
    //fragColor = vec4(c);
    fragColor = vec4(m, 1.);
}