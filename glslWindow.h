#pragma once

#include <iostream>
#ifdef __APPLE__
    #include <SDL.h> // For macOS
#else
    #include <SDL2/SDL.h> // For other platforms
#endif
// Include GLAD
#include <glad/glad.h>
// #include "tinyfiledialogs.h"
#include <fstream>
#include <sstream>
#include <string>
#include <filesystem>
#include <vector>

#include <filesystem>
#include "imgui.h"

GLuint LoadShader(const char* shaderCode, GLenum shaderType);
void InitGLSLContext(SDL_Window*& glslWindow, SDL_GLContext& glslContext, 
            std::filesystem::path shaderPath, int windowWidth, int windowHeight);
GLuint CreateShaderProgram(const char* vertexShaderCode, const char* fragmentShaderCode);
void HandleGlslWindowEvents(SDL_Event& event, bool* isRunning, SDL_Window* glslWindow, int& windowWidth, int& windowHeight, 
            float& mouseX, float& mouseY, float& mouseClickX, float& mouseClickY, bool& mouseDown, GLuint iResolutionLocation, 
            bool* ctr_mouse, bool* ctr_play, GLint& iTimeLocation);
float GetElapsedTime(Uint32* reset_time);
// void SetCurrentTime(float currentTime);
void RenderGLSLWindow(SDL_Window* window, GLuint shaderProgram, int& windowWidth, int& windowHeight, 
            float& mouseX, float& mouseY, float& mouseClickX, float& mouseClickY, bool& mouseDown, 
            GLuint iResolutionLocation, GLuint iMouseLocation, GLuint iTimeLocation, GLuint* VAO, 
            Uint32* reset_time, bool* ctr_play);

void StartGLSLWindow(SDL_Window* glslWindow, SDL_GLContext glslContext, int windowWidth, int windowHeight, 
            GLuint shaderProgram, SDL_Window* menuWindow, SDL_GLContext menuContext, ImGuiContext* imGUIContext, 
            std::string& openFilename);

void CleanupGLSL(SDL_Window* glslWindow, SDL_GLContext glslContext, GLuint * VAO, GLuint * VBO);
