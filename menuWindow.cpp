#include "glslWindow.h"
#include "menuWindow.h"

// #include "nfd.h"    // TODO fix mingw

const char* glsl_version = "#version 150";

void InitializeImGui(SDL_Window*& menuWindow, SDL_GLContext& menuContext, ImGuiContext*& imGuiContext) {
   // Initialize SDL
    if (SDL_Init(SDL_INIT_VIDEO) != 0) {
        std::cout << "SDL could not initialize! SDL_Error: " << SDL_GetError() << std::endl;
        return;
    }

    // Create SDL Window
    menuWindow = SDL_CreateWindow("Controls", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
                240, 560, SDL_WINDOW_OPENGL);
    if (!menuWindow) {
        std::cout << "Window could not be created! SDL_Error: " << SDL_GetError() << std::endl;
        return;
    }
    // SDL_SetWindowResizable(menuWindow, SDL_TRUE);

    // Create SDL OpenGL Context
    menuContext = SDL_GL_CreateContext(menuWindow);
    if (!menuContext) {
        std::cout << "OpenGL context could not be created! SDL_Error: " << SDL_GetError() << std::endl;
        return;
    }

    // SDL context to menuContext
    SDL_GL_MakeCurrent(menuWindow, menuContext);

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER,1);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE,24);

    // setup GLAD
    // SDL_GL_MakeCurrent(glslWindow, glslContext);
    gladLoadGLLoader(SDL_GL_GetProcAddress);

    // Initialize SDL and specify OpenGL version
    // (Assuming you have initialized SDL elsewhere)

    const GLubyte* glVersion = glGetString(GL_VERSION);
    std::cout << "menuWindow: OpenGL Version: " << glVersion << std::endl;
    
    // Initialize ImGui
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO(); (void)io;

    // Initialize ImGui SDL and OpenGL implementations
    ImGui_ImplSDL2_InitForOpenGL(menuWindow, menuContext);
    ImGui_ImplOpenGL3_Init("#version 150");  // Assuming you are using OpenGL 3.2. Adjust if necessary.

    // Optional: Setup ImGui styles
    ImGui::StyleColorsDark();

    imGuiContext = ImGui::GetCurrentContext();

    std::cout << "ImGui initialized. Context: " << imGuiContext << std::endl;

    // SDL_GL_SwapWindow(menuWindow);
}

void HandleMenuWindowEvents(SDL_Event& event, bool* isRunning, SDL_Window* menuWindow) {
    if(event.type == SDL_QUIT){  // TODO fix: is never called
        std::cout << "SDL_QUIT event\n";
        *isRunning = false;
    } 
    else if (event.type == SDL_WINDOWEVENT) {
        if (event.window.event == SDL_WINDOWEVENT_CLOSE) {
            // Close menu window
            std::cout << "Closing menu window\n";
            SDL_DestroyWindow(menuWindow);
            menuWindow = NULL; // or set some flag to note it's closed
            *isRunning = false;
        } 
        else if (event.window.event == SDL_WINDOWEVENT_RESIZED) {  // Not resizable, but just in case
            // Get new dimensions and resize the OpenGL viewport
            int windowWidth = event.window.data1;
            int windowHeight = event.window.data2;
            glViewport(0, 0, windowWidth, windowHeight);

            // Update the iResolution uniform
            // glUniform2f(iResolutionLocation, (float)windowWidth, (float)windowHeight);  // TODO get resolution 
        }
    }
    else {
        ImGui_ImplSDL2_ProcessEvent(&event);  // imgui ui change
    }
}

void RenderMenuWindow(SDL_Window* menuWindow, SDL_GLContext menuContext, ImGuiContext* imGuiContext, 
            bool* ctr_play, bool* ctr_mouse, Uint32* reset_time, bool* isRunning, std::string& openFilename) {

    // SDL context to menuContext
    SDL_GL_MakeCurrent(menuWindow, menuContext);

    // InitializeImGUI(menuWindow, menuContext, imGuiContext);

    // std::cerr << "ImGui context: " << ImGui::GetCurrentContext() << std::endl;
    // const GLubyte* glslVersion = glGetString(GL_SHADING_LANGUAGE_VERSION);
    // std::cerr << "GLSL version: " << glslVersion << std::endl;
    // gladLoadGLLoader((GLADloadproc) SDL_GL_GetProcAddress);

    // Set ImGui context
    // std::cout << "imGuiContext render: " << imGuiContext << std::endl;
    ImGui::SetCurrentContext(imGuiContext);

    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplSDL2_NewFrame(menuWindow);
    ImGui::NewFrame();

    {
    // Begin a fullscreen window (covers the entire screen)
    ImGui::SetNextWindowPos(ImVec2(0, 0));
    ImGui::SetNextWindowSize(ImGui::GetIO().DisplaySize);
    ImGui::Begin("Controls", nullptr, ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove);

    // Control
    ImGui::Checkbox("Activate mouse interaction", ctr_mouse);
    // Create checkboxes
    ImGui::Checkbox("Play animation", ctr_play);
    // ImGui::Button("Reset animation", ImVec2(ImGui::GetWindowWidth(), 0.0f));

    // More ImGui widgets can go here...
    // iTime "reset"
    if (ImGui::Button("Reset animation")) {
        *reset_time = SDL_GetTicks() / 1000.0f;  // Store the current tick count.
        // std::cout << "Reset time in menuWindow: " << *reset_time << std::endl;
        // float currentTime = GetElapsedTime(reset_time);
        // SetCurrentTime(currentTime);
    }

    // imgui separator
    ImGui::Separator();

    if (ImGui::Button("Open shader file")) {
        OpenFileDialog(openFilename, isRunning);    // TODO fix mingw
    }

    // End the window
    ImGui::End();
    }

    // glUseProgram(0);
    ImGui::Render();
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
}

 void OpenFileDialog(std::string& openFilename, bool* isRunning) {
     nfdchar_t *outPath = NULL;
     const nfdchar_t *defaultDir = openFilename.c_str();
     nfdresult_t result = NFD_OpenDialog( "frag;glsl", defaultDir, &outPath );
     if ( result == NFD_OKAY )
     {
         puts("Success!");
         puts(outPath);
         openFilename = outPath;
         free(outPath);
         std::cout << "Selected file: " << openFilename << std::endl;
         *isRunning = false;
     }
     else if ( result == NFD_CANCEL )
     {
         puts("User pressed cancel.");
     }
     else 
     {
         printf("Error: %s\n", NFD_GetError() );
     }
 }

void DeleteImGui() {
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplSDL2_Shutdown();
    ImGui::DestroyContext();
}
