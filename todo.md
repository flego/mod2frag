BEFORE DEPLOYING
=====

- [x] bundle frameworks in local directory and build with ``-rpath @loader_path/Frameworks``
- [x] macos automate build
- [x] cross compile to linux / win
- [ ] lin + win: file reader
- [ ] lin + win: file picker interface fix 
- [ ] win: fix unicode representation
- [ ] document build instructions
- [ ] write something about it on gitlab
- [ ] models: cgns import
- [ ] upload rendering to shadertoy button


COMPILE HINTS FOR LINUX (GENTOO)
=====
## linux build
```
clang++ main.cpp menuWindow.cpp glslWindow.cpp ./glad/src/glad.c ./imgui-1.89.9/imgui.cpp ./imgui-1.89.9/imgui_demo.cpp ./imgui-1.89.9/imgui_draw.cpp ./imgui-1.89.9/imgui_tables.cpp ./imgui-1.89.9/imgui_widgets.cpp ./imgui-1.89.9/backends/imgui_impl_opengl3.cpp ./imgui-1.89.9/backends/imgui_impl_sdl2.cpp  -o mod2frag -std=c++17 -lSDL2 -ldl -I /usr/portage/media-libs/libsdl2/files -I ./glad/include -I ./imgui-1.89.9 -I ./imgui-1.89.9/backends -I/usr/include/gtk-3.0 -I/usr/include/pango-1.0 -I/usr/include/glib-2.0 -I/usr/lib64/glib-2.0/include -I/usr/lib64/libffi/include -I/usr/include/fribidi -I/usr/include/cairo -I/usr/include/pixman-1 -I/usr/include/freetype2 -I/usr/include/uuid -I/usr/include/libpng16 -I/usr/include/harfbuzz -I/usr/include/gdk-pixbuf-2.0 -I/usr/include/libmount -I/usr/include/blkid -I/usr/include/gio-unix-2.0 -I/usr/include/atk-1.0 -I/usr/include/at-spi2-atk/2.0 -I/usr/include/dbus-1.0 -I/usr/lib64/dbus-1.0/include -I/usr/include/at-spi-2.0 -pthread -lgtk-3 -lgdk-3 -lpangocairo-1.0 -lpango-1.0 -latk-1.0 -lcairo-gobject -lcairo -lgdk_pixbuf-2.0 -lgio-2.0 -lgobject-2.0 -lglib-2.0 -v
```

## mingw sdl source paths on macos:
```
/usr/local/i686-w64-mingw32
/usr/local/x86_64-w64-mingw32
```

## mingw sdl source paths on macos:
```
/usr/local/i686-w64-mingw32
/usr/local/x86_64-w64-mingw32
```

List LC_RPATH
=====
```
otool -l build/Mod2Frag.app/Contents/MacOS/mod2frag | grep -A2 LC_RPATH
List dependent libraries
otool -L build/Mod2Frag.app/Contents/MacOS/mod2frag
List
```

GLSL shader code example
=====

```
todos

- [ ] scalar field visualization of return float functions

glsl useful functions

// =====                                          // initialization
vec2 st = gl_FragCoord.xy/u_resolution;
// =====

float y = pow(st.x,exp);                         // gradients
// st.x             // direction
// exp             // width, intensity
// exp < -1.     // invert
// exp = 1.     // linear gradient
// exp = .0     // no gradient

float y = step(g,st.x);                             // steps
// g             // threshold
// st.x             // direction

float y = smoothstep(a,b,st.x);                // gradients btn 2 stps
// a                // start gradient
// b                // end gradient
// st.x            // direction

float y = smoothstep(0.2,0.5,st.x) - smoothstep(0.5,0.8,st.x);
                                                // draw line

// =====                                         // coloring
vec3 color = vec3(y);
// shorthand for
// vec3 color = vec3(y,y,y); 
// =====

// =====                                        // output
gl_FragColor = vec4(color,1.0);
// =====


// ++++++++ FUNCTIONS +++++++

// "ShaderToy Tutorial - CameraSystem" 
// by Martijn Steinrucken aka BigWings/CountFrolic - 2017
// License Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.

float DistLine(vec3 ro, vec3 rd, vec3 p) {
    return length(cross(p-ro, rd))/length(rd);
}

float DrawPoint(vec3 ro, vec3 rd, vec3 p) {
    float d = DistLine(ro, rd, p);
        d = smoothstep(.06, .05, d);
        return d;
}
```

SDL event example
=====
```
(SDL_Event) {
  type = 32512
  common = (type = 32512, timestamp = 14194)
  display = {
    type = 32512
    timestamp = 14194
    display = 0
    event = '\0'
    padding1 = '\0'
    padding2 = '\0'
    padding3 = '\0'
    data1 = 0
  }
  window = {
    type = 32512
    timestamp = 14194
    windowID = 0
    event = '\0'
    padding1 = '\0'
    padding2 = '\0'
    padding3 = '\0'
    data1 = 0
    data2 = 0
  }
  key = {
    type = 32512
    timestamp = 14194
    windowID = 0
    state = '\0'
    repeat = '\0'
    padding2 = '\0'
    padding3 = '\0'
    keysym = (scancode = SDL_SCANCODE_UNKNOWN, sym = 0, mod = 0, unused = 0)
  }
  edit = (type = 32512, timestamp = 14194, windowID = 0, text = "", start = 0, length = 0)
  editExt = (type = 32512, timestamp = 14194, windowID = 0, text = 0x0000000000000000, start = 0, length = 0)
  text = (type = 32512, timestamp = 14194, windowID = 0, text = "")
  motion = (type = 32512, timestamp = 14194, windowID = 0, which = 0, state = 0, x = 0, y = 0, xrel = 0, yrel = 0)
  button = {
    type = 32512
    timestamp = 14194
    windowID = 0
    which = 0
    button = '\0'
    state = '\0'
    clicks = '\0'
    padding1 = '\0'
    x = 0
    y = 0
  }
  wheel = {
    type = 32512
    timestamp = 14194
    windowID = 0
    which = 0
    x = 0
    y = 0
    direction = 0
    preciseX = 0
    preciseY = 0
    mouseX = 0
    mouseY = 0
  }
  jaxis = {
    type = 32512
    timestamp = 14194
    which = 0
    axis = '\0'
    padding1 = '\0'
    padding2 = '\0'
    padding3 = '\0'
    value = 0
    padding4 = 0
  }
  jball = {
    type = 32512
    timestamp = 14194
    which = 0
    ball = '\0'
    padding1 = '\0'
    padding2 = '\0'
    padding3 = '\0'
    xrel = 0
    yrel = 0
  }
  jhat = (type = 32512, timestamp = 14194, which = 0, hat = '\0', value = '\0', padding1 = '\0', padding2 = '\0')
  jbutton = (type = 32512, timestamp = 14194, which = 0, button = '\0', state = '\0', padding1 = '\0', padding2 = '\0')
  jdevice = (type = 32512, timestamp = 14194, which = 0)
  jbattery = (type = 32512, timestamp = 14194, which = 0, level = SDL_JOYSTICK_POWER_EMPTY)
  caxis = {
    type = 32512
    timestamp = 14194
    which = 0
    axis = '\0'
    padding1 = '\0'
    padding2 = '\0'
    padding3 = '\0'
    value = 0
    padding4 = 0
  }
  cbutton = (type = 32512, timestamp = 14194, which = 0, button = '\0', state = '\0', padding1 = '\0', padding2 = '\0')
  cdevice = (type = 32512, timestamp = 14194, which = 0)
  ctouchpad = (type = 32512, timestamp = 14194, which = 0, touchpad = 0, finger = 0, x = 0, y = 0, pressure = 0)
  csensor = {
    type = 32512
    timestamp = 14194
    which = 0
    sensor = 0
    data = ([0] = 0, [1] = 0, [2] = 0)
    timestamp_us = 0
  }
  adevice = {
    type = 32512
    timestamp = 14194
    which = 0
    iscapture = '\0'
    padding1 = '\0'
    padding2 = '\0'
    padding3 = '\0'
  }
  sensor = {
    type = 32512
    timestamp = 14194
    which = 0
    data = ([0] = 0, [1] = 0, [2] = 0, [3] = 0, [4] = 0, [5] = 0)
    timestamp_us = 0
  }
  quit = (type = 32512, timestamp = 14194)
  user = (type = 32512, timestamp = 14194, windowID = 0, code = 0, data1 = 0x0000000000000000, data2 = 0x0000000000000000)
  syswm = {
    type = 32512
    timestamp = 14194
    msg = nullptr
  }
  tfinger = (type = 32512, timestamp = 14194, touchId = 0, fingerId = 0, x = 0, y = 0, dx = 0, dy = 0, pressure = 0, windowID = 0)
  mgesture = (type = 32512, timestamp = 14194, touchId = 0, dTheta = 0, dDist = 0, x = 0, y = 0, numFingers = 0, padding = 0)
  dgesture = (type = 32512, timestamp = 14194, touchId = 0, gestureId = 0, numFingers = 0, error = 0, x = 0, y = 0)
  drop = (type = 32512, timestamp = 14194, file = 0x0000000000000000, windowID = 0)
  padding = "\0\U0000007f\0\0r7"
}
```
