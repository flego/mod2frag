OUT = mod2frag
C   = clang
CC  = clang++
export MACOSX_DEPLOYMENT_TARGET = 10.15
WINCC = x86_64-w64-mingw32-g++

SRC_C    = $(wildcard *.c) ./glad/src/glad.c ./nativefiledialog/src/nfd_common.c 
SRC_CPP  = $(wildcard *.cpp) $(wildcard ./imgui-1.89.9/*.cpp) \
			$(wildcard ./imgui-1.89.9/backends/imgui_impl_opengl3.cpp ./imgui-1.89.9/backends/imgui_impl_sdl2.cpp)
SRC_OBJC = $(wildcard *.m) ./nativefiledialog/src/nfd_cocoa.m
SRC_WIN_CPP	= ./nativefiledialog/src/nfd_win.cpp
SRC_LIN_C   = ./nativefiledialog/src/nfd_common.c ./nativefiledialog/src/nfd_gtk.c
SRC_LIN_CPP = main.cpp menuWindow.cpp glslWindow.cpp ./glad/src/glad.c ./imgui-1.89.9/imgui.cpp ./imgui-1.89.9/imgui_demo.cpp ./imgui-1.89.9/imgui_draw.cpp ./imgui-1.89.9/imgui_tables.cpp ./imgui-1.89.9/imgui_widgets.cpp ./imgui-1.89.9/backends/imgui_impl_opengl3.cpp ./imgui-1.89.9/backends/imgui_impl_sdl2.cpp   -lSDL2 -ldl -lgtk-3 -lgdk-3

INCLUDE = -I ./SDL2.framework/Headers -I ./glad/include -I ./imgui-1.89.9 -I ./imgui-1.89.9/backends -I ./nativefiledialog/src -I ./nativefiledialog/src/include
FRAMEWORKS = -F /Library/Frameworks -framework SDL2 -framework AppKit -framework CoreFoundation
RPATH = -rpath @executable_path/../Frameworks
IFR = $(INCLUDE) $(FRAMEWORKS) $(RPATH)
INCLUDEADDLINUX = -I /usr/portage/media-libs/libsdl2/files -I ./glad/include -I ./imgui-1.89.9 -I ./imgui-1.89.9/backends -I/usr/include/gtk-3.0 -I/usr/include/pango-1.0 -I/usr/include/glib-2.0 -I/usr/lib64/glib-2.0/include -I/usr/lib64/libffi/include -I/usr/include/fribidi -I/usr/include/cairo -I/usr/include/pixman-1 -I/usr/include/freetype2 -I/usr/include/uuid -I/usr/include/libpng16 -I/usr/include/harfbuzz -I/usr/include/gdk-pixbuf-2.0 -I/usr/include/libmount -I/usr/include/blkid -I/usr/include/gio-unix-2.0 -I/usr/include/atk-1.0 -I/usr/include/at-spi2-atk/2.0 -I/usr/include/dbus-1.0 -I/usr/lib64/dbus-1.0/include -I/usr/include/at-spi-2.0 -pthread -lgtk-3 -lgdk-3 -lpangocairo-1.0 -lpango-1.0 -latk-1.0 -lcairo-gobject -lcairo -lgdk_pixbuf-2.0 -lgio-2.0 -lgobject-2.0 -lglib-2.0
SDL = ./SDL2.framework
MINGW_PATH = /usr/local/x86_64-w64-mingw32
MINGW_INCLUDE = $(MINGW_PATH)/include
MINGW_LIB = $(MINGW_PATH)/lib

MACOSARMOBJ = $(SRC_C:.c=.macosarm.o) $(SRC_CPP:.cpp=.macosarm.o) $(SRC_OBJC:.m=.macosarm.o)
MACOSX64OBJ = $(SRC_C:.c=.macosx64.o) $(SRC_CPP:.cpp=.macosx64.o) $(SRC_OBJC:.m=.macosx64.o)
WINX64OBJ   = $(SRC_C:.c=.win64.o) $(SRC_CPP:.cpp=.win64.o) $(SRC_WIN_CPP:.cpp=.win64.o)
LINX64OBJ   = $(SRC_LIN_C:.c=.linx64.o)
OBJ         = $(MACOSARMOBJ) $(MACOSX64OBJ) $(WINX64OBJ) $(LINX64OBJ)


.PHONY: run_macos all clean
all: run_macos_x64

%.macosarm.o: %.c
	$(C) $(IFR) -c $< -o $@ -g
%.macosarm.o: %.m
	$(C) $(IFR) -c $< -o $@ -g
%.macosarm.o: %.cpp
	$(CC) -std=c++17 $(IFR) -c $< -o $@ -g
link_macos_arm: $(MACOSARMOBJ)
	$(CC) $(MACOSARMOBJ) -o $(OUT) $(IFR)
debug_macos_arm: $(MACOSARMOBJ)
	$(CC) $(MACOSARMOBJ) -o $(OUT) $(IFR) -g -O0
bundle_macos_arm: link_macos_arm
	mkdir -p "./build/$(OUT).app"/Contents/{MacOS,Frameworks,Resources}
	cp -R "$(SDL)" "./build/$(OUT).app/Contents/Frameworks/"
	cp Info.plist "./build/$(OUT).app/Contents/"
	cp ./$(OUT) "./build/$(OUT).app/Contents/MacOS/$(OUT)"
	cp mod2frag.icns "./build/$(OUT).app/Contents/Resources/"
	cp -r ./shaderfiles "./build/$(OUT).app/Contents/Resources/"
	cp vertex_shader.glsl "./build/$(OUT).app/Contents/Resources/"
run_macos_arm: bundle_macos_arm
	open ./build/$(OUT).app

%.macosx64.o: %.c
	$(C) -arch x86_64 $(IFR) -c $< -o $@ -g
%.macosx64.o: %.m
	$(C) -arch x86_64 $(IFR) -c $< -o $@ -g
%.macosx64.o: %.cpp
	$(CC) -arch x86_64 -std=c++17 $(IFR) -c $< -o $@ -g
link_macos_x64: $(MACOSX64OBJ)
	$(CC) -arch x86_64 $(MACOSX64OBJ) -o $(OUT) $(IFR)
debug_macos_x64: $(MACOSX64OBJ)
	$(CC) -arch x86_64 $(MACOSX64OBJ) -o $(OUT) $(IFR) -g -O0
bundle_macos_x64: link_macos_x64
	mkdir -p "./build/$(OUT).app"/Contents/{MacOS,Frameworks,Resources}
	cp -R "$(SDL)" "./build/$(OUT).app/Contents/Frameworks/"
	cp Info.plist "./build/$(OUT).app/Contents/"
	cp ./$(OUT) "./build/$(OUT).app/Contents/MacOS/$(OUT)"
	cp mod2frag.icns "./build/$(OUT).app/Contents/Resources/"
	cp -r ./shaderfiles "./build/$(OUT).app/Contents/Resources/"
	cp vertex_shader.glsl "./build/$(OUT).app/Contents/Resources/"
run_macos_x64: bundle_macos_x64
	open ./build/$(OUT).app

%.win64.o: %.c
	$(WINCC) -I $(MINGW_INCLUDE) $(INCLUDE) -c $< -o $@ -g
%.win64.o: %.cpp
	$(WINCC) -std=c++17 -I $(MINGW_INCLUDE) $(INCLUDE) -c $< -o $@ -g
link_win_x64: $(WINX64OBJ)
	$(WINCC) $(WINX64OBJ) -o ./build/$(OUT) -L $(MINGW_LIB) \
	-static-libgcc -static-libstdc++ \
	-Wl,-Bstatic -lstdc++ -lwinpthread -lSDL2main -lSDL2 -Wl,-Bdynamic \
	-lglu32 -lopengl32 -lwinmm -lole32 -loleaut32 -limm32 -luuid \
	-mwindows -lsetupapi -lversion -lcfgmgr32 \
	 $(INCLUDE) 
bundle_win_x64: link_win_x64
	mkdir -p "./build/$(OUT)-win"
#	cp -r "$(MINGW_PATH)/bin/SDL2.dll" "./build/$(OUT)-win/"
#	cp -r "$(MINGW_PATH)/bin/libstdc++-6.dll" "./build/$(OUT)-win/"
	cp "./build/$(OUT).exe" "./build/$(OUT)-win/"
	cp -r ./shaderfiles "./build/$(OUT)-win/"
	cp vertex_shader.glsl "./build/$(OUT)-win/"
	zip -r "./build/$(OUT)-win.zip" "./build/$(OUT)-win"
# -l SDL2_image

%.linx64.o: %.c
	$(C) $(INCLUDE) $(INCLUDEADDLINUX) -c $< -o $@ -v
build_linux_x64: $(LINX64OBJ)
	$(CC) -std=c++17 $(SRC_LIN_CPP) $(LINX64OBJ) -o $(OUT) -ldl $(INCLUDE) $(INCLUDEADDLINUX) -v
debug_linux_x64: $(LINX64OBJ)
	$(CC) -std=c++17 $(SRC_LIN_CPP) $(LINX64OBJ) -o $(OUT) -ldl $(INCLUDE) $(INCLUDEADDLINUX) -v -g -O0
run_linux_x64: build_linux_x64
	./$(OUT)

clean:
	rm -f $(OUT) $(OBJ)
	rm -rf ./build/*
	rm -f imgui.ini

