#include "glslWindow.h"
#include "menuWindow.h"

// GLint* iTimeLocation = new GLint;

GLuint LoadShader(const char* shaderCode, GLenum shaderType) {
    GLuint shader = glCreateShader(shaderType);
    glShaderSource(shader, 1, &shaderCode, nullptr);
    glCompileShader(shader);

    GLint compileStatus;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &compileStatus);
    if (!compileStatus) {
        // Handle compile error...
        return -1;
    }

    return shader;
}

void InitGLSLContext(SDL_Window*& glslWindow, SDL_GLContext& glslContext, 
            std::filesystem::path shaderPath, int windowWidth, int windowHeight) {
    // Initialize the video subsystem.
    // If it returns less than 1, then an
    // error code will be received.
    if(SDL_Init(SDL_INIT_VIDEO) < 0){
        std::cout << "glsl: SDL could not be initialized: " <<
                  SDL_GetError();
    }else{
        std::cout << "glsl: SDL video system is ready to go\n";
    }
    // Before we create our window, specify OpenGL version
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER,1);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE,24);

    // glslWindow = SDL_CreateWindow(shaderPath.c_str(),  // TODO fix mingw
    glslWindow = SDL_CreateWindow("shader [mingw]",
        20,
        20,
        windowWidth,
        windowHeight,
        SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL); 
    SDL_SetWindowResizable(glslWindow, SDL_TRUE);
    glslContext = SDL_GL_CreateContext(glslWindow);

    // setup GLAD
    gladLoadGLLoader(SDL_GL_GetProcAddress);

    const GLubyte* glVersion = glGetString(GL_VERSION);
    std::cout << "glslWindow: OpenGL Version: " << glVersion << std::endl;
}

GLuint CreateShaderProgram(const char* vertexShaderCode, const char* fragmentShaderCode) {
    // Create shader programs
    GLuint vertexShader = LoadShader(vertexShaderCode, GL_VERTEX_SHADER);
    GLuint fragmentShader = LoadShader(fragmentShaderCode, GL_FRAGMENT_SHADER);

    // if fragment shader does not start with "#version", abort
    if (fragmentShader < 0) {
        std::cout << "Error: fragment shader doesn't seem to be a valid shader file.\n";
        return -1;
    }

    GLuint program = glCreateProgram();
    glAttachShader(program, vertexShader);
    glAttachShader(program, fragmentShader);
    glLinkProgram(program);

    GLint linkStatus;
    glGetProgramiv(program, GL_LINK_STATUS, &linkStatus);
    if (!linkStatus) {
        // GLint maxLength = 0;
        // glGetShaderiv(program, GL_INFO_LOG_LENGTH, &maxLength);

        // // The maxLength includes the NULL character
        // std::vector<GLchar> errorLog(maxLength);
        // glGetShaderInfoLog(program, maxLength, &maxLength, &errorLog[0]);

        // // Log the error
        // std::cerr << &errorLog[0] << std::endl;
	std::cerr << "glGetProgramiv error" << std::endl;

        // Terminate, as the shader is not usable
        glDeleteProgram(program);
        return -1;
    }

    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);

    return program;
}

void HandleGlslWindowEvents(SDL_Event& event, bool* isRunning, SDL_Window* glslWindow, int& windowWidth, int& windowHeight, 
            float& mouseX, float& mouseY, float& mouseClickX, float& mouseClickY, bool& mouseDown, GLuint iResolutionLocation, 
            bool* ctr_mouse, bool* ctr_play, GLint& iTimeLocation) {

    if (event.type == SDL_WINDOWEVENT) {
        if (event.window.event == SDL_WINDOWEVENT_CLOSE) { 
            std::cout << "Closing shader window\n";
            *isRunning = false;
        } 
        else if (event.window.event == SDL_WINDOWEVENT_RESIZED) {
            // Get new dimensions and resize the OpenGL viewport
            windowWidth = event.window.data1;
            windowHeight = event.window.data2;
            glViewport(0, 0, windowWidth, windowHeight);

            // Update the iResolution uniform
            glUniform2f(iResolutionLocation, (float)windowWidth, (float)windowHeight);
        }
    } else if (event.type == SDL_MOUSEBUTTONDOWN && *ctr_mouse) {
        mouseClickX = event.button.x;
        mouseClickY = event.button.y;
        mouseDown = true;
    } else if (event.type == SDL_MOUSEBUTTONUP && *ctr_mouse) {
        mouseDown = false;
    } else if (event.type == SDL_MOUSEMOTION && *ctr_mouse) {
        if (mouseDown) {
            mouseX = event.motion.x;
            mouseY = event.motion.y;
        }
    }
    // else if (!ctr_play) {
        // std::cout << "ctr_play: " << ctr_play << std::endl;
        // float currentTime = SDL_GetTicks() / 1000.0f;
    // }
}

float GetElapsedTime(Uint32* reset_time) {
    return SDL_GetTicks() / 1000.0f - *reset_time;  // convert milliseconds to seconds, subtract reset_time
}

// void SetCurrentTime(float currentTime) {
//     glUniform1f(*iTimeLocation, currentTime);  // call after init in StartGLSLWindow
// }

void RenderGLSLWindow(SDL_Window* window, GLuint shaderProgram, int& windowWidth, int& windowHeight, 
            float& mouseX, float& mouseY, float& mouseClickX, float& mouseClickY, bool& mouseDown, 
            GLuint iResolutionLocation, GLuint iMouseLocation, GLuint iTimeLocation, GLuint* VAO, 
            Uint32* reset_time, bool* ctr_play) {
    if (*ctr_play) {
        float currentTime = GetElapsedTime(reset_time);
        glUniform1f(iTimeLocation, currentTime);  // TODO init once in set shader uniforms below ?
    }
    // else {
    //     reset_time +=    // TODO add delta to reset_time
    // }

    glUseProgram(shaderProgram);

    // Track mouse movement
    glUniform4f(iMouseLocation, mouseX, windowHeight - mouseY, mouseClickX, windowHeight - mouseClickY);  // TODO hight width from variables

    // Set shader uniforms
    glUniform2f(iResolutionLocation, windowWidth, windowHeight);  

    // Render full-screen quad
    glBindVertexArray(*VAO);
    // std::cout << "GL_TRIANGLE_STRIP: " << GL_TRIANGLE_STRIP << "\n";

    // print gl error
    GLenum err = glGetError();
    if(err != GL_NO_ERROR) {
        std::cerr << "OpenGL error before glDrawArrays: " << err << std::endl;
    }
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
}

void StartGLSLWindow(SDL_Window* glslWindow, SDL_GLContext glslContext, int windowWidth, int windowHeight, GLuint shaderProgram, 
            SDL_Window* menuWindow, SDL_GLContext menuContext, ImGuiContext* imGUIContext, std::string& openFilename) {
    // Setup our function pointers
    GLint iResolutionLocation = glGetUniformLocation(shaderProgram, "iResolution");
    GLint iTimeLocation = glGetUniformLocation(shaderProgram, "iTime");
    GLint iMouseLocation = glGetUniformLocation(shaderProgram, "iMouse");

    // Vertex data for a fullscreen quad
    float quadVertices[] = {
        -1.0f,  1.0f,
        1.0f,  1.0f,
        -1.0f, -1.0f,
        1.0f, -1.0f
    };

    // Setup VAO/VBO
    GLuint * VAO, * VBO;    // TODO move to init
    VAO = new GLuint;
    VBO = new GLuint;
    glGenVertexArrays(1, VAO);
    glGenBuffers(1, VBO);
    glBindVertexArray(*VAO);
    glBindBuffer(GL_ARRAY_BUFFER, *VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), quadVertices, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(float), (void*)0);

    // Initialize mouse
    float mouseX = 1.0f, mouseY = 1.0f;
    float mouseClickX = 0.0f, mouseClickY = 0.0f;
    bool mouseDown = false;

    // Infinite loop for our application
    bool isRunning = true;
    // imgui controls
    bool ctr_play = true;  // TODO: init from menuWindow
    bool ctr_mouse = true;
    Uint32 reset_time = 0.0f;  // is subtracted from SDL_GetTicks() to reset time
    SDL_Event event;
    glViewport(0,0,windowWidth,windowHeight);

    while(isRunning){
        // MENU RENDERING
        // Drawing code for menu
        SDL_GL_MakeCurrent(menuWindow, menuContext);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);  // clear buffers
        RenderMenuWindow(menuWindow, menuContext, imGUIContext, &ctr_play, &ctr_mouse, 
                    &reset_time, &isRunning, openFilename);
        // Update window with OpenGL rendering
        SDL_GL_SwapWindow(menuWindow);

        // GLSL RENDERING
        // Drawing code for GLSL
        SDL_GL_MakeCurrent(glslWindow, glslContext);
        glEnableVertexAttribArray(0);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);  // clear buffers
        RenderGLSLWindow(glslWindow, shaderProgram, windowWidth, windowHeight, 
            mouseX, mouseY, mouseClickX, mouseClickY, mouseDown, iResolutionLocation, 
            iMouseLocation, iTimeLocation, VAO, &reset_time, &ctr_play);
        glDisableVertexAttribArray(0);
        // Update windows with OpenGL rendering
        SDL_GL_SwapWindow(glslWindow);

        // EVENT HANDLING
        // Start our event loop
        while(SDL_PollEvent(&event)) {
            if (event.window.windowID == SDL_GetWindowID(glslWindow)) {
                SDL_GL_MakeCurrent(glslWindow, glslContext);
                HandleGlslWindowEvents(event, &isRunning, glslWindow, windowWidth, windowHeight, 
                    mouseX, mouseY, mouseClickX, mouseClickY, mouseDown, iResolutionLocation, 
                    &ctr_mouse, &ctr_play, iTimeLocation);
            }
            // TODO: menuWindow.cpp calls from here
            if (event.window.windowID == SDL_GetWindowID(menuWindow)) {
                SDL_GL_MakeCurrent(menuWindow, menuContext);
                HandleMenuWindowEvents(event, &isRunning, menuWindow);
            }
        }
    }

    // CLEANUP
    CleanupGLSL(glslWindow, glslContext, VAO, VBO);
    glDeleteProgram(shaderProgram);
}

void CleanupGLSL(SDL_Window* glslWindow, SDL_GLContext glslContext, GLuint * VAO, GLuint * VBO) {
    glDeleteVertexArrays(1, VAO);
    glDeleteBuffers(1, VBO);
    delete VAO;
    delete VBO;
    // delete iTimeLocation;

    SDL_GL_DeleteContext(glslContext);
    SDL_DestroyWindow(glslWindow);
}