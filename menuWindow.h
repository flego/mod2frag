#pragma once

#include <iostream>
#ifdef __APPLE__
    #include <SDL.h> // For macOS
#else
    #include <SDL2/SDL.h> // For other platforms
#endif
// Include GLAD
#include <glad/glad.h>
#include "nfd.h"
#include <fstream>
#include <sstream>
#include <string>
#include <filesystem>
#include "imgui.h"
#include "imgui_impl_sdl2.h"
#include "imgui_impl_opengl3.h"

void HandleMenuWindowEvents(SDL_Event& event, bool* isRunning, SDL_Window* menuWindow);
void InitializeImGui(SDL_Window*& menuWindow, SDL_GLContext& menuContext, ImGuiContext*& imGuiContext);
void RenderMenuWindow(SDL_Window* menuWindow, SDL_GLContext menuContext, ImGuiContext* imGuiContext, 
            bool* ctr_play, bool* ctr_mouse, Uint32* reset_time, bool* isRunning, std::string& openFilename);
// bool getCtrPlay();
// bool getCtrMouse();
void OpenFileDialog(std::string& openFilename, bool* isRunning);
void DeleteImGui();
